<?php

namespace Drupal\commerce_order_document\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the order document plugin annotation object.
 *
 * Plugin namespace: Plugin\Commerce\OrderDocument.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class CommerceOrderDocument extends Plugin {

  use StringTranslationTrait;

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The order document label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The order document display label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $display_label;

}
