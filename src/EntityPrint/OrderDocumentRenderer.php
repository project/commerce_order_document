<?php

namespace Drupal\commerce_order_document\EntityPrint;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order_document\Entity\OrderDocumentInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_print\Renderer\RendererBase;

/**
 * A renderer for content entities.
 */
class OrderDocumentRenderer extends RendererBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function render(array $entities) {
    // First entity should be order document; additional should be orders to be rendered.
    $order_document = array_shift($entities);
    assert($order_document instanceof OrderDocumentInterface);
    $build = [];
    foreach ($entities as $order) {
      assert($order instanceof OrderInterface);
      $build[] = $order_document->getPlugin()->buildOrderDocument($order, TRUE);
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilename(array $entities) {
    // First entity should be order document; additional should be orders to be rendered.
    $order_document = array_shift($entities);
    assert($order_document instanceof OrderDocumentInterface);
    $plugin = $order_document->getPlugin();
    $entities_label = $this->filenameGenerator->generateFilename($entities, static function (OrderInterface $order) {
      return $order->id();
    });
    return $this->t('Order @id @document', [
      '@id' => $entities_label,
      '@document' => $plugin->getDisplayLabel(),
    ]);
  }

}
