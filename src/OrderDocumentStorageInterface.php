<?php

namespace Drupal\commerce_order_document;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Defines the interface for order document storage.
 */
interface OrderDocumentStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Loads all eligible order documents for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\commerce_order_document\Entity\OrderDocumentInterface[]
   *   The order documents.
   */
  public function loadMultipleForOrder(OrderInterface $order);

}
