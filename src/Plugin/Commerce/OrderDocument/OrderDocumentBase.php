<?php

namespace Drupal\commerce_order_document\Plugin\Commerce\OrderDocument;

use Drupal\commerce\MailHandlerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderTotalSummaryInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface;
use Drupal\entity_print\PrintBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Provides the base class for order documents.
 */
abstract class OrderDocumentBase extends PluginBase implements OrderDocumentInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The plugin manager for our print engines.
   *
   * @var \Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface
   */
  protected $entityPrintPluginManager;

  /**
   * The print builder.
   *
   * @var \Drupal\entity_print\PrintBuilderInterface
   */
  protected $printBuilder;

  /**
   * The mail handler.
   *
   * @var \Drupal\commerce\MailHandlerInterface
   */
  protected $mailHandler;

  /**
   * The order total summary.
   *
   * @var \Drupal\commerce_order\OrderTotalSummaryInterface
   */
  protected $orderTotalSummary;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The parent config entity.
   *
   * Not available while the plugin is being configured.
   *
   * @var \Drupal\commerce_order_document\Entity\OrderDocumentInterface
   */
  protected $parentEntity;

  /**
   * Constructs a new OrderDocumentBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface $plugin_manager
   *   The entity print plugin manager.
   * @param \Drupal\entity_print\PrintBuilderInterface $print_builder
   *   The print builder.
   * @param \Drupal\commerce\MailHandlerInterface $mail_handler
   *   The mail handler.
   * @param \Drupal\commerce_order\OrderTotalSummaryInterface $order_total_summary
   *   The order total summary.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory, AccountInterface $current_user, EntityPrintPluginManagerInterface $plugin_manager, PrintBuilderInterface $print_builder, MailHandlerInterface $mail_handler, OrderTotalSummaryInterface $order_total_summary, Token $token) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->entityPrintPluginManager = $plugin_manager;
    $this->printBuilder = $print_builder;
    $this->mailHandler = $mail_handler;
    $this->orderTotalSummary = $order_total_summary;
    $this->token = $token;

    if (array_key_exists('_entity', $configuration)) {
      $this->parentEntity = $configuration['_entity'];
      unset($configuration['_entity']);
    }
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('plugin.manager.entity_print.print_engine'),
      $container->get('entity_print.print_builder'),
      $container->get('commerce.mail_handler'),
      $container->get('commerce_order.order_total_summary'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayLabel() {
    return $this->configuration['display_label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {

    return [
      'display_label' => $this->pluginDefinition['display_label'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['display_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display name'),
      '#description' => $this->t('Shown on order documents form and used for download filename.'),
      '#default_value' => $this->configuration['display_label'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration = [];
      $this->configuration['display_label'] = $values['display_label'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function readyForOrder(OrderInterface $order) {
    return ($order->getState()->getId() != 'draft');
  }

  /**
   * {@inheritdoc}
   */
  public function canDownloadDocument(OrderInterface $order) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function downloadDocument(OrderInterface $order) {
    $entities = [$this->parentEntity, $order];
    // Create the Print engine plugin.
    $print_engine = $this->entityPrintPluginManager->createSelectedInstance('pdf');
    $config = $this->configFactory->get('entity_print.settings');

    return (new StreamedResponse(function () use ($entities, $print_engine, $config) {
      // The Print is sent straight to the browser.
      $this->printBuilder->deliverPrintable($entities, $print_engine, $config->get('force_download'), $config->get('default_css'));
    }))->send();
  }

  /**
   * {@inheritdoc}
   */
  public function canEmailDocument(OrderInterface $order) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrderConfirmOptions(OrderInterface $order) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sendDocumentEmail(OrderInterface $order, array $option_values = NULL) {
    if (empty($to = $this->getEmailTo($order))) {
      return FALSE;
    }
    if (empty($subject = $this->getEmailSubject($order))) {
      return FALSE;
    }
    if (empty($body = $this->buildOrderDocument($order))) {
      return FALSE;
    }
    return $this->mailHandler->sendMail($to, $subject, $body, $this->getEmailParams($order));
  }

  /**
   * {@inheritdoc}
   */
  public function sendOnOrderTransitionId(OrderInterface $order) {
    return '';
  }

  /**
   * Gets to for email.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return string
   *   The email to.
   */
  protected function getEmailTo(OrderInterface $order) {
    return $order->getEmail();
  }

  /**
   * Gets subject for email.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return string
   *   The email subject.
   */
  protected function getEmailSubject(OrderInterface $order) {
    return $this->t('@store order #@number @label', [
      '@store' => $order->getStore()->label(),
      '@number' => $order->getOrderNumber(),
      '@label' => $this->getDisplayLabel(),
    ]);
  }

  /**
   * Gets parameters for email..
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   The email parameters.
   */
  protected function getEmailParams(OrderInterface $order) {
    return [
      'id' => 'order_document_' . $this->parentEntity->id(),
      'from' => $order->getStore()->getEmailFromHeader(),
      'order' => $order,
    ];
  }

}
