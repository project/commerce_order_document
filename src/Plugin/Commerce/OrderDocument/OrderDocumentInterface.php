<?php

namespace Drupal\commerce_order_document\Plugin\Commerce\OrderDocument;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the base interface for order documents.
 */
interface OrderDocumentInterface extends ConfigurableInterface, PluginFormInterface {

  /**
   * Gets the order document admin-facing label.
   *
   * @return mixed
   *   The order document label.
   */
  public function getLabel();

  /**
   * Gets the order document display label.
   *
   * @return string
   *   The order document display label.
   */
  public function getDisplayLabel();

  /**
   * Checks whether the document is ready for the order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return bool
   *   TRUE if the document can be build, FALSE otherwise.
   */
  public function readyForOrder(OrderInterface $order);

  /**
   * Builds the order document.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param bool $entity_print
   *   Whether entity print template should be used.
   *
   * @return array
   *   A render array containing the order document.
   */
  public function buildOrderDocument(OrderInterface $order, $entity_print = FALSE);

  /**
   * Checks whether a document for the given order can be downloaded.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return bool
   *   TRUE if the document can be downloaded, FALSE otherwise.
   */
  public function canDownloadDocument(OrderInterface $order);

  /**
   * Prints order document as a pdf.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object on error otherwise the Print is sent.
   */
  public function downloadDocument(OrderInterface $order);

  /**
   * Checks whether a document for the given order can be emailed.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return bool
   *   TRUE if the document can be emailed, FALSE otherwise.
   */
  public function canEmailDocument(OrderInterface $order);

  /**
   * Builds options for email confirmation form.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   A render array containing email option form elements.
   */
  public function buildOrderConfirmOptions(OrderInterface $order);

  /**
   * Sends the document to the order customer.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $option_values
   *   An associative array of values submitted to the confirmation form.
   *
   * @return bool
   *   TRUE if the email was sent successfully, FALSE otherwise.
   */
  public function sendDocumentEmail(OrderInterface $order, array $option_values = NULL);

  /**
   * Gets the order transition id for sending document to customer.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return string
   *   The order transition id.
   */
  public function sendOnOrderTransitionId(OrderInterface $order);

}
