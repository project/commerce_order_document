<?php

namespace Drupal\commerce_order_document\Plugin\Commerce\OrderDocument;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Receipt order document.
 *
 * @CommerceOrderDocument(
 *   id = "receipt",
 *   label = "Receipt",
 *   display_label = "Receipt",
 * )
 */
class Receipt extends OrderDocumentBase implements OrderDocumentInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'send_receipt' => FALSE,
      'receipt_bcc' => '',
      'receipt_subject' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['send_receipt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Email the customer a receipt when an order is placed'),
      '#default_value' => $this->configuration['send_receipt'],
    ];
    $form['receipt_bcc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Send a copy of the receipt to this email:'),
      '#default_value' => $this->configuration['receipt_bcc'],
      '#states' => [
        'visible' => [
          ':input[name="configuration[receipt][send_receipt]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $default_subject = $this->t('Order #@number confirmed', [
      '@number' => '[commerce_order:order_number]',
    ]);
    $form['receipt_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject for the order receipt email:'),
      '#description' => $this->t('Leave blank to use default value: @default_subject', [
        '@default_subject' => $default_subject,
      ]),
      '#default_value' => $this->configuration['receipt_subject'],
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['commerce_order'],
    ];
    $form['receipt_subject_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['commerce_order'],
      '#global_types' => FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['send_receipt'] = $values['send_receipt'];
      $this->configuration['receipt_bcc'] = $values['receipt_bcc'];
      $this->configuration['receipt_subject'] = $values['receipt_subject'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrderDocument(OrderInterface $order, $entity_print = FALSE) {
    $theme = 'commerce_order_receipt';
    if ($entity_print) {
      $theme = $theme . '__entity_print';
    }
    $body = [
      '#theme' => $theme,
      '#order_entity' => $order,
      '#totals' => $this->orderTotalSummary->buildTotals($order),
    ];
    if ($billing_profile = $order->getBillingProfile()) {
      $profile_view_builder = $this->entityTypeManager->getViewBuilder('profile');
      $body['#billing_information'] = $profile_view_builder->view($billing_profile);
    }

    return $body;
  }

  /**
   * {@inheritdoc}
   */
  public function canDownloadDocument(OrderInterface $order) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function canEmailDocument(OrderInterface $order) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function sendDocumentEmail(OrderInterface $order, array $option_values = NULL) {
    $subject = $this->configuration['receipt_subject'];
    if (!empty($subject)) {
      $subject = $this->token->replace($subject, [
        'commerce_order' => $order,
      ]);
    }
    // Provide a default value if the subject line was blank.
    if (empty($subject)) {
      $subject = $this->t('Order #@number confirmed', ['@number' => $order->getOrderNumber()]);
    }
    if (empty($body = $this->buildOrderDocument($order))) {
      return FALSE;
    }

    $params = [
      'id' => 'order_receipt',
      'from' => $order->getStore()->getEmailFromHeader(),
      'bcc' => $this->configuration['receipt_bcc'],
      'order' => $order,
    ];
    return $this->mailHandler->sendMail($order->getEmail(), $subject, $body, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function sendOnOrderTransitionId(OrderInterface $order) {
    if ($this->configuration['send_receipt']) {
      return 'place';
    }
    return '';
  }

}
