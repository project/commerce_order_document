<?php

namespace Drupal\commerce_order_document\Plugin\Commerce\OrderDocument;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Default order document.
 *
 * @CommerceOrderDocument(
 *   id = "default",
 *   label = "Default",
 *   display_label = "Order Document",
 * )
 */
class DefaultOrderDocument extends OrderDocumentBase implements OrderDocumentInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'can_download' => TRUE,
      'can_email' => TRUE,
      'email_subject' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['can_download'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Administrative users can download document'),
      '#default_value' => $this->configuration['can_download'],
    ];
    $form['can_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Administrative users can email document'),
      '#default_value' => $this->configuration['can_email'],
    ];

    $default_subject = $this->t('@store order #@number', [
      '@store' => '[commerce_order:store_id:entity:name]',
      '@number' => '[commerce_order:order_number]',
    ]);
    $form['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject for the order document email:'),
      '#description' => $this->t('Leave blank to use default value: @default_subject', [
        '@default_subject' => $default_subject,
      ]),
      '#default_value' => $this->configuration['email_subject'],
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['commerce_order'],
    ];
    $form['email_subject_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['commerce_order'],
      '#global_types' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['can_download'] = $values['can_download'];
      $this->configuration['can_email'] = $values['can_email'];
      $this->configuration['email_subject'] = $values['email_subject'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrderDocument(OrderInterface $order, $entity_print = FALSE) {
    $theme = 'commerce_order_document';
    if ($entity_print) {
      $theme = 'commerce_order_document__' . $this->parentEntity->id() . '__entity_print';
    }

    $body = [
      '#theme' => $theme,
      '#document_id' => $this->parentEntity->id(),
      '#order_entity' => $order,
      '#totals' => $this->orderTotalSummary->buildTotals($order),
      '#site_path' => Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString(),
    ];

    $profiles = $order->collectProfiles();
    if (!empty($profiles)) {
      $body['#order_profiles'] = [];
      $profile_view_builder = $this->entityTypeManager->getViewBuilder('profile');
      $order_profiles = [];
      foreach ($profiles as $profile_id => $profile) {
        $body['#order_profiles'][$profile_id] = $profile_view_builder->view($profile);
      }
    }

    if ($this->moduleHandler->moduleExists('commerce_shipping')) {
      $summary = \Drupal::service('commerce_shipping.order_shipment_summary')->build($order);
      if (!empty($summary)) {
        $body['#shipping_information'] = $summary;
      }
    }

    if ($this->moduleHandler->moduleExists('commerce_payment')) {
      /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
      $payment_gateway = $order->get('payment_gateway')->entity;
      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = $order->get('payment_method')->entity;

      // The payment_method variable represents the selected payment option.
      // Uses the payment gateway display label if payment methods are not
      // supported, matching the logic in PaymentOptionsBuilder::buildOptions().
      $body['#payment_method'] = NULL;
      if ($payment_method) {
        $body['#payment_method'] = [
          '#markup' => $payment_method->label(),
        ];
      }
      elseif ($payment_gateway) {
        $payment_gateway_plugin = $payment_gateway->getPlugin();
        if (!($payment_gateway_plugin instanceof SupportsStoredPaymentMethodsInterface)) {
          $body['#payment_method'] = [
            '#markup' => $payment_gateway_plugin->getDisplayLabel(),
          ];
        }
      }
    }

    return $body;
  }

  /**
   * {@inheritdoc}
   */
  public function canDownloadDocument(OrderInterface $order) {
    return $this->configuration['can_download'];
  }

  /**
   * {@inheritdoc}
   */
  public function canEmailDocument(OrderInterface $order) {
    return $this->configuration['can_email'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrderConfirmOptions(OrderInterface $order) {
    $form = [];
    if (!empty($copy = $this->currentUser->getEmail())) {
      $form['copy_me'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Also send order @label email to @copy.', [
          '@label' => $this->configuration['display_label'],
          '@copy' => $copy,
        ]),
        '#default_value' => TRUE,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function sendDocumentEmail(OrderInterface $order, array $option_values = NULL) {
    $subject = $this->configuration['email_subject'];
    // Ensure subject and body.
    if (empty($subject) || empty($body = $this->buildOrderDocument($order))) {
      return FALSE;
    }
    $subject = $this->token->replace($subject, [
      'commerce_order' => $order,
    ]);

    $to = $order->getEmail();
    if (!empty($option_values) && !empty($option_values['copy_me'])) {
      $copy = $this->currentUser->getEmail();
      $to = implode(', ', array_filter([$to, $copy]));
    }

    $params = [
      'id' => 'order_document_' . $this->parentEntity->id(),
      'from' => $order->getStore()->getEmailFromHeader(),
      'order' => $order,
    ];
    return $this->mailHandler->sendMail($to, $subject, $body, $params);
  }

}
