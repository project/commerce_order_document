<?php

namespace Drupal\commerce_order_document\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sends order document emails on order event transitions.
 */
class OrderEventEmailSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OrderReceiptSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = ['commerce_order.post_transition' => ['onOrderTransition', -100]];
    return $events;
  }

  /**
   * Sends any applicable order document emails.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event we subscribed to.
   */
  public function onOrderTransition(WorkflowTransitionEvent $event) {
    $transition_id = $event->getTransition()->getId();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $order_document_storage = $this->entityTypeManager->getStorage('commerce_order_document');
    /** @var \Drupal\commerce_order_document\Entity\OrderDocumentInterface[] $documents */
    $documents = $order_document_storage->loadMultipleForOrder($order);
    foreach ($documents as $document) {
      $plugin = $document->getPlugin();
      if ($plugin->sendOnOrderTransitionId($order) == $transition_id) {
        $plugin->sendDocumentEmail($order);
      }
    }
  }

}
