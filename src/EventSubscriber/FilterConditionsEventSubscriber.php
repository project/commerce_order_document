<?php

namespace Drupal\commerce_order_document\EventSubscriber;

use Drupal\commerce\Event\FilterConditionsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Removes unnecessary conditions on the order document configuration form.
 */
class FilterConditionsEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce.filter_conditions' => 'onFilterConditions',
    ];
    return $events;
  }

  /**
   * Removes unneeded conditions.
   *
   * Order documents has order type field used for filtering,
   *  so there's no need to have conditions targeting the same data.
   *
   * @param \Drupal\commerce\Event\FilterConditionsEvent $event
   *   The event.
   */
  public function onFilterConditions(FilterConditionsEvent $event) {
    if ($event->getParentEntityTypeId() == 'commerce_order_document') {
      $definitions = $event->getDefinitions();
      unset($definitions['order_type']);
      $event->setDefinitions($definitions);
    }
  }

}
