<?php

namespace Drupal\commerce_order_document\Event;

use Drupal\commerce\EventBase;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the event for filtering the available order documents.
 *
 * @see \Drupal\commerce_order_document\Event\DocumentEvents
 */
class FilterOrderDocumentsEvent extends EventBase {

  /**
   * The order documents.
   *
   * @var \Drupal\commerce_order_document\Entity\OrderDocumentInterface[]
   */
  protected $orderDocuments;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Constructs a new FilterOrderDocumentsEvent object.
   *
   * @param \Drupal\commerce_order_document\Entity\OrderDocumentInterface[] $order_documents
   *   The order documents.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function __construct(array $order_documents, OrderInterface $order) {
    $this->orderDocuments = $order_documents;
    $this->order = $order;
  }

  /**
   * Gets the order documents.
   *
   * @return \Drupal\commerce_order_document\Entity\OrderDocumentInterface[]
   *   The order documents.
   */
  public function getOrderDocuments() {
    return $this->orderDocuments;
  }

  /**
   * Sets the order documents.
   *
   * @param \Drupal\commerce_order_document\Entity\OrderDocumentInterface[] $order_documents
   *   The order documents.
   *
   * @return $this
   */
  public function setOrderDocuments(array $order_documents) {
    $this->orderDocuments = $order_documents;
    return $this;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder() {
    return $this->order;
  }

}
