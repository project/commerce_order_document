<?php

namespace Drupal\commerce_order_document\Event;

/**
 * Defines events for the order document module.
 */
final class DocumentEvents {

  /**
   * Name of the event fired when order documents are loaded for an order.
   *
   * @Event
   *
   * @see \Drupal\commerce_order_document\Event\FilterOrderDocumentsEvent
   */
  const FILTER_ORDER_DOCUMENTS = 'commerce_order_document.filter_order_documents';

}
