<?php

namespace Drupal\commerce_order_document\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides an order document controller.
 */
class OrderDocumentController extends ControllerBase {

  /**
   * Outputs the order document view for an order.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return array
   *   The render array.
   */
  public function view(RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    $order_document_id = $route_match->getParameter('order_document');

    /** @var \Drupal\commerce_order_document\OrderDocumentStorageInterface $order_document_storage */
    $order_document_storage = $this->entityTypeManager()->getStorage('commerce_order_document');
    $order_document = $order_document_storage->load($order_document_id);

    $plugin = $order_document->getPlugin();
    return $plugin->buildOrderDocument($order);
  }

  /**
   * Prints order document as a pdf.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object on error otherwise the Print is sent.
   */
  public function download(RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    $order_document_id = $route_match->getParameter('order_document');

    /** @var \Drupal\commerce_order_document\OrderDocumentStorageInterface $order_document_storage */
    $order_document_storage = $this->entityTypeManager()->getStorage('commerce_order_document');
    $order_document = $order_document_storage->load($order_document_id);

    $plugin = $order_document->getPlugin();
    return $plugin->downloadDocument($order);
  }

}
