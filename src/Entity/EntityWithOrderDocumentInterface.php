<?php

namespace Drupal\commerce_order_document\Entity;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the interface for entities managed by an order document.
 */
interface EntityWithOrderDocumentInterface extends EntityInterface {

  /**
   * Gets the order document.
   *
   * @return \Drupal\commerce_order_document\Entity\OrderDocumentInterface|null
   *   The order document entity, or null if unknown.
   */
  public function getOrderDocument();

  /**
   * Gets the order document ID.
   *
   * @return string|null
   *   The order document ID, or null if unknown.
   */
  public function getOrderDocumentId();

}
