<?php

namespace Drupal\commerce_order_document\Entity;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Defines the interface for order document configuration entities.
 *
 * Stores configuration for order document plugins.
 */
interface OrderDocumentInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Gets the target entity type ID.
   *
   * This is the entity type for which the pattern will be used.
   * For example, "commerce_order".
   *
   * @return string
   *   The target entity type ID.
   */
  public function getTargetEntityTypeId();

  /**
   * Sets the target entity type ID.
   *
   * @param string $entity_type_id
   *   The target entity type ID.
   *
   * @return $this
   */
  public function setTargetEntityTypeId($entity_type_id);

  /**
   * Gets the order document weight.
   *
   * @return string
   *   The order document weight.
   */
  public function getWeight();

  /**
   * Sets the order document weight.
   *
   * @param int $weight
   *   The order document weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Gets the order document order type.
   *
   * @return string
   *   The order document order type.
   */
  public function getOrderType();

  /**
   * Gets the order document plugin.
   *
   * @return \Drupal\commerce_order_document\Plugin\Commerce\OrderDocument\OrderDocumentInterface
   *   The order document plugin.
   */
  public function getPlugin();

  /**
   * Gets the order document plugin ID.
   *
   * @return string
   *   The order document plugin ID.
   */
  public function getPluginId();

  /**
   * Sets the order document plugin ID.
   *
   * @param string $plugin_id
   *   The order document plugin ID.
   *
   * @return $this
   */
  public function setPluginId($plugin_id);

  /**
   * Gets the order document plugin configuration.
   *
   * @return array
   *   The order document plugin configuration.
   */
  public function getPluginConfiguration();

  /**
   * Sets the order document plugin configuration.
   *
   * @param array $configuration
   *   The porder document plugin configuration.
   *
   * @return $this
   */
  public function setPluginConfiguration(array $configuration);

  /**
   * Gets the order document conditions.
   *
   * @return \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface[]
   *   The order document conditions.
   */
  public function getConditions();

  /**
   * Gets the order document condition operator.
   *
   * @return string
   *   The condition operator. Possible values: AND, OR.
   */
  public function getConditionOperator();

  /**
   * Sets the order document condition operator.
   *
   * @param string $condition_operator
   *   The condition operator.
   *
   * @return $this
   */
  public function setConditionOperator($condition_operator);

  /**
   * Checks whether the order document applies to the given order.
   *
   * Ensures that the conditions pass.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return bool
   *   TRUE if order document applies, FALSE otherwise.
   */
  public function applies(OrderInterface $order);

}
