<?php

namespace Drupal\commerce_order_document;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order_document\Event\FilterOrderDocumentsEvent;
use Drupal\commerce_order_document\Event\DocumentEvents;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines the order document storage.
 */
class OrderDocumentStorage extends ConfigEntityStorage implements OrderDocumentStorageInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a OrderDocumentStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(EntityTypeInterface $entity_type, ConfigFactoryInterface $config_factory, UuidInterface $uuid_service, LanguageManagerInterface $language_manager, MemoryCacheInterface $memory_cache, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($entity_type, $config_factory, $uuid_service, $language_manager, $memory_cache);

    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('config.factory'),
      $container->get('uuid'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultipleForOrder(OrderInterface $order) {
    /** @var \Drupal\commerce_order_document\Entity\OrderDocumentInterface[] $order_documents */
    $order_documents = $this->loadByProperties([
      'status' => TRUE,
      'orderType' => $order->bundle(),
    ]);

    // Allow the list of order documents to be filtered via code.
    $event = new FilterOrderDocumentsEvent($order_documents, $order);
    $this->eventDispatcher->dispatch($event, DocumentEvents::FILTER_ORDER_DOCUMENTS);
    $order_documents = $event->getOrderDocuments();
    // Evaluate conditions for the remaining ones.
    foreach ($order_documents as $order_document_id => $order_document) {
      $plugin = $order_document->getPlugin();
      if (empty($plugin) || !$plugin->readyForOrder($order)) {
        unset($order_documents[$order_document_id]);
      }
      elseif (!$order_document->applies($order)) {
        unset($order_documents[$order_document_id]);
      }
    }
    uasort($order_documents, [$this->entityType->getClass(), 'sort']);
    return $order_documents;
  }

}
