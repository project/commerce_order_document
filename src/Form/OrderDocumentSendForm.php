<?php

namespace Drupal\commerce_order_document\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a confirmation form for sending order documents.
 */
class OrderDocumentSendForm extends ContentEntityConfirmFormBase {

  /**
   * The order doument plugin.
   *
   * @var \Drupal\commerce_order_document\Plugin\Commerce\OrderDocument\OrderDocumentInterface
   */
  protected $document;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to send the order %label?', [
      '%label' => $this->document->getDisplayLabel(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Send document');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('commerce_order_document.order_documents_page', [
      'commerce_order' => $this->entity->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $order_document = '') {
    $order_document_storage = $this->entityTypeManager->getStorage('commerce_order_document');
    $order_document = $order_document_storage->load($order_document);
    $this->document = $order_document->getPlugin();

    $form = parent::buildForm($form, $form_state);

    if (!empty($options = $this->document->buildOrderConfirmOptions($this->entity))) {
      foreach ($options as $id => $element) {
        $form[$id] = $element;
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entity;
    $result = $this->document->sendDocumentEmail($order, $form_state->getValues());

    // Drupal's MailManager sets an error message itself, if the sending failed.
    if ($result) {
      $this->messenger()->addMessage($this->t('Order @label sent.', [
        '@label' => $this->document->getDisplayLabel(),
      ]));
    }
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
