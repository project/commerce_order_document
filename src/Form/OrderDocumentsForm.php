<?php

namespace Drupal\commerce_order_document\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the order documents form.
 */
class OrderDocumentsForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The document options.
   *
   * @var array
   */
  protected $documentOptions;

  /**
   * Constructs a new OrderDocumentsForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match) {
    $this->entityTypeManager = $entity_type_manager;
    $this->order = $route_match->getParameter('commerce_order');

    /** @var \Drupal\commerce_order_document\OrderDocumentStorageInterface $order_document_storage */
    $order_document_storage = $entity_type_manager->getStorage('commerce_order_document');
    $order_documents = $order_document_storage->loadMultipleForOrder($this->order);

    $this->documentOptions = [];
    foreach ($order_documents as $order_document_id => $order_document) {
      $plugin = $order_document->getPlugin();
      $this->documentOptions[$order_document_id] = [
        'id' => $order_document_id,
        'label' => $plugin->getDisplayLabel(),
        'download' => $plugin->canDownloadDocument($this->order),
        'email' => $plugin->canEmailDocument($this->order),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_order_documents_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (empty($this->documentOptions)) {
      $form['warning'] = [
        '#markup' => $this->t('No documents are currently available for this order.'),
      ];
      return $form;
    }

    $wrapper_id = Html::getUniqueId('order-documents-form-wrapper');
    $form['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['#suffix'] = '</div>';
    $form['#tree'] = TRUE;

    $default_option = reset($this->documentOptions);
    // The form state will have a document_option value if #ajax was used.
    if (!empty($option_id = $form_state->getValue('document_option'))) {
      if (isset($this->documentOptions[$option_id])) {
        $default_option = $this->documentOptions[$option_id];
      }
    }

    $option_labels = array_column($this->documentOptions, 'label', 'id');
    $form['document_option'] = [
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this->t('Document'),
      '#options' => $option_labels,
      '#default_value' => $default_option['id'],
      '#ajax' => [
        'callback' => '::ajaxRefresh',
        'wrapper' => $wrapper_id,
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions', 
    ];
    $form['actions']['view'] = [
      '#type' => 'submit',
      '#value' => $this->t('View Document'),
      '#attributes' => [
        'onclick' => 'this.form.target="_blank";return true;',
      ],
      '#submit_button' => 'view',
    ];
    $form['actions']['download'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download Document'),
      '#disabled' => !$default_option['download'],
      '#submit_button' => 'download',
    ];
    $form['actions']['email'] = [
      '#type' => 'submit',
      '#value' => $this->t('Email Document'),
      '#disabled' => !$default_option['email'],
      '#submit_button' => 'email',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $submit_type = $triggering_element['#submit_button'];
    $document_option = $form_state->getValue(['document_option']);

    $form_state->setRedirect('commerce_order_document.order_document_' . $submit_type, [
      'commerce_order' => $this->order->id(),
      'order_document' => $document_option,
    ]);
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    return $form;
  }

}
