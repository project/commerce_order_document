<?php

namespace Drupal\commerce_order_document;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the list builder for order documents.
 */
class OrderDocumentListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $entitiesKey = 'documents';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_order_documents';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['id'] = $this->t('ID');
    $header['display_label'] = $this->t('Display label');
    $header['plugin'] = $this->t('Plugin');
    $header['order_type'] = $this->t('Order type');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_order_document\Entity\OrderDocumentInterface $entity */
    $order_document_plugin = $entity->getPlugin();
    $type = $order_document_plugin->getLabel();
    $status = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['label'] = $entity->label();
    // $this->weightKey determines whether the table will be rendered as a form.
    if (!empty($this->weightKey)) {
      $row['id']['#markup'] = $entity->id();
      $row['display_label']['#markup'] = $order_document_plugin->getDisplayLabel();
      $row['plugin']['#markup'] = $type;
      $row['order_type']['#markup'] = $entity->getOrderType();
      $row['status']['#markup'] = $status;
    }
    else {
      $row['id'] = $entity->id();
      $row['display_label'] = $order_document_plugin->getDisplayLabel();
      $row['plugin'] = $type;
      $row['order_type'] = $entity->getOrderType();
      $row['status'] = $status;
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $entities = $this->load();
    // If there are less than 2 documents, disable dragging.
    if (count($entities) <= 1) {
      unset($this->weightKey);
    }
    return parent::render();
  }

}
